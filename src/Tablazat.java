import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileOutputStream;

public class Tablazat extends JFrame{

   private JTable jt;
   private JScrollPane sp;
   private JPopupMenu jpm;
   private JMenuItem excel;
   private JMenuItem pdf;

    Tablazat(String adoszam, String sql) {
        super("Dolgozói listák");

        DB db = new DB();

        String column[] = {"Neve", "Anyja neve", "Állampolgár","Város","Cím","Adóazonosító","Tajszám","Bruttó","Cég Adószáma"};
        jt = new JTable(db.showAllDolgJTable(adoszam,sql), column);
        jt.setBounds(30, 40, 400, 300);
        sp = new JScrollPane(jt);
        add(sp);

        jpm = new JPopupMenu("Menü");
        excel = new JMenuItem("Mentés excel-be");
        pdf = new JMenuItem("Mentés pdf-be");
        jpm.add(excel);
        jpm.add(pdf);

        jt.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3){
                    jpm.show(e.getComponent(), e.getX(), e.getY());


                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        excel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filePath = db.fileChooser();
               if(db.fileNameCheck(filePath)==false){
                db.exportXls(jt,filePath);}
            }
        });

        pdf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String filePath = db.fileChooser();

                if(db.fileNameCheck(filePath)==false){
                    pdfWrite(filePath);}


            }
        });

    }
    @Deprecated
    /**Ne használd nem túl jó*/
    private void printToPdf(String filepathName) {
        Document document = new Document(PageSize.A4.rotate());
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filepathName+".pdf"));

            document.open();
            PdfContentByte cb = writer.getDirectContent();

            cb.saveState();
            Graphics2D g2 = cb.createGraphicsShapes(500, 500);

            Shape oldClip = g2.getClip();
            g2.clipRect(0, 0, 500, 500);

            jt.print(g2);
            g2.setClip(oldClip);

            g2.dispose();
            cb.restoreState();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        document.close();
    }

    /**JTable pdf-be írása*/
    public void pdfWrite(String fileNamePath){
        try{
            int count=jt.getRowCount();
            Document document=new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document,new FileOutputStream(fileNamePath+".pdf"));
            document.open();
            PdfPTable tab=new PdfPTable(9);

            tab.addCell("Neve");
            tab.addCell("Anyja neve");
            tab.addCell("Állampolgár");
            tab.addCell("Város");
            tab.addCell("Cím");
            tab.addCell("Adóazonosító");
            tab.addCell("Tajszám");
            tab.addCell("Bruttó");
            tab.addCell("Cég Adószáma");

            for(int i=0;i<count;i++){
                Object obj1 = GetData(jt, i, 0);
                Object obj2 = GetData(jt, i, 1);
                Object obj3 = GetData(jt, i, 2);
                Object obj4 = GetData(jt, i, 3);
                Object obj5 = GetData(jt, i, 4);
                Object obj6 = GetData(jt, i, 5);
                Object obj7 = GetData(jt, i, 6);
                Object obj8 = GetData(jt, i, 7);
                Object obj9 = GetData(jt, i, 8);

                String value1=obj1.toString();
                String value2=obj2.toString();
                String value3=obj3.toString();
                String value4=obj4.toString();
                String value5=obj5.toString();
                String value6=obj6.toString();
                String value7=obj7.toString();
                String value8=obj8.toString();
                String value9=obj9.toString();

                tab.addCell(value1);
                tab.addCell(value2);
                tab.addCell(value3);
                tab.addCell(value4);
                tab.addCell(value5);
                tab.addCell(value6);
                tab.addCell(value7);
                tab.addCell(value8);
                tab.addCell(value9);

            }
            document.add(tab);
            document.close();
            Desktop.getDesktop().open(new File(fileNamePath + ".pdf"));
        }
        catch(Exception e){}
    }
    public Object GetData(JTable table, int row_index, int col_index){
        return table.getModel().getValueAt(row_index, col_index);
    }

}
