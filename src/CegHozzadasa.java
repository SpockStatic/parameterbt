import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CegHozzadasa extends JFrame {

   private JLabel cegnevLabel;
   private JLabel varosLabel;
   private JLabel cimLabel;
   private JLabel adoszamLabel;
   private JTextField cegnevText;
   private JTextField varosText;
   private JTextField cimText;
   private JTextField adoszamText;
   private JButton hozzaad;
   private JButton clear;


    public CegHozzadasa(){
        super("Cégek hozzáadása");
        setLayout(null);

        cegnevLabel = new JLabel("Cégnév: ");
        cegnevLabel.setBounds(10,10,100,30);
        add(cegnevLabel);

        varosLabel = new JLabel("Város: ");
        varosLabel.setBounds(10,60,100,30);
        add(varosLabel);

        cimLabel = new JLabel("Cim: ");
        cimLabel.setBounds(10,110,100,30);
        add(cimLabel);

        adoszamLabel = new JLabel("Adóazonosító:");
        adoszamLabel.setBounds(10,160,100,30);
        add(adoszamLabel);

        cegnevText = new JTextField();
        cegnevText.setBounds(120,10,300,30);
        add(cegnevText);

        varosText = new JTextField();
        varosText.setBounds(120,60,300,30);
        add(varosText);

        cimText = new JTextField();
        cimText.setBounds(120,110,300,30);
        add(cimText);

        adoszamText = new JTextField();
        adoszamText.setBounds(120,160,300,30);
        add(adoszamText);

        hozzaad=new JButton("Hozzáadás");
        hozzaad.setBounds(75,210,125,30);
        add(hozzaad);

        clear = new JButton("Mezők Törlése");
        clear.setBounds(225,210,125,30);
        add(clear);

        hozzaad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String cegnev=cegnevText.getText();
                String varos=varosText.getText();
                String cim=cimText.getText();
                String adoszam=adoszamText.getText();

                if(!cegnev.isEmpty() && !varos.isEmpty() && !cim.isEmpty() && !adoszam.isEmpty()
                        && !cegnev.equals("") && !varos.equals("") && !cim.equals("") && !adoszam.equals("") ) {
                    DB db = new DB();

                    db.addCeg(cegnev, varos, cim, adoszam);
                    JOptionPane.showMessageDialog(null," A "+cegnev+" hozzá lett adva az adatbázishoz");
                }else JOptionPane.showMessageDialog(null,"Az összes mező kitőltése kötelező");

            }
        });


        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cegnevText.setText(null);
                varosText.setText(null);
                cimText.setText(null);
                adoszamText.setText(null);
            }
        });

    }

}
