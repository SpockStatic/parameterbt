import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DolgozoLista extends JFrame {

private JComboBox cegeValaszto;
private JScrollPane scrollPane;
private JLabel cegNevLabel;
private JLabel tol;
private JLabel ig;
private JLabel line;
private JTextArea dolgText;
private JTextField fizTol;
private JTextField fizIg;
private JButton cegOk;
private JButton pdf;
private JButton nevRend;
private JButton fizRend;
private JButton fizKozott;
private JButton tablazat;

String osszDolgozo;
String cegAdoszam;
    String sql;

    public DolgozoLista(String adoszam, String cegNev){
        super(cegNev + " alkalmazottainka listázása");
        setLayout(null);

        DB db = new DB();


        cegNevLabel = new JLabel(cegNev +"     "+adoszam);
        cegNevLabel.setBounds(10,10,275,30);
        add(cegNevLabel);

       /* cegeValaszto = new JComboBox(db.cegArray());
        cegeValaszto.setBounds(315,10,145,30);
        add(cegeValaszto);*/

<<<<<<< HEAD
        cegOk = new JButton("Listáz");
=======
        cegOk = new JButton("Kiválaszt");
>>>>>>> main
        cegOk.setBounds(475,10,145,30);
        add(cegOk);

        dolgText = new JTextArea("");
        JScrollPane scrollPane = new JScrollPane(dolgText);
        scrollPane.setBounds(10,60,450,400);
        dolgText.setFont(dolgText.getFont().deriveFont(16f));
        add(scrollPane);

        nevRend = new JButton("Név szerint rendez");
        nevRend.setBounds(475,60,145,30);
        add(nevRend);

        fizRend = new JButton("Bér szerint rendez");
        fizRend.setBounds(475,110,145,30);
        add(fizRend);

        fizKozott = new JButton("Bér szűrése");
        fizKozott.setBounds(475,260,145,30);
        add(fizKozott);

        pdf = new JButton("Mentés");
        pdf.setBounds(475,430,145,30);
        add(pdf);

        fizTol = new JTextField();
        fizTol.setBounds(475,160,100,30);
        add(fizTol);

        fizIg = new JTextField();
        fizIg.setBounds(475,210,100,30);
        add(fizIg);

        line = new JLabel("- - - - - - - - - - - - - - - - - - - -");
        line.setBounds(475,130,145,30);
        add(line);

        tol = new JLabel("-tól");
        tol.setBounds(600,160,30,30);
        add(tol);

        ig = new JLabel("-ig");
        ig.setBounds(600,210,30,30);
        add(ig);

        tablazat = new JButton("Táblázat nézet");
        tablazat.setBounds(475,380,145,30);
        add(tablazat);



        cegOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dolgText.setText("");
                sql ="select * from dolgozo where cegadoszam=?";
                cegAdoszam=db.cegAdoszam(cegNev);


                System.out.println(cegNev+"   "+cegAdoszam);

                String metaDolg=db.showMetaDolg();
                osszDolgozo=db.showAllDolg(cegAdoszam,sql);

               // cegNevLabel.setText(cegNev+"    Adószám:"+cegAdoszam);
               // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                dolgText.append(osszDolgozo);

            }
        });

        nevRend.addActionListener(new ActionListener() {
<<<<<<< HEAD
=======
            @Override
            public void actionPerformed(ActionEvent e) {
                dolgText.setText("");
                sql ="select * from dolgozo where cegadoszam=? order by nev";
                cegAdoszam=db.cegAdoszam(cegNev);

                System.out.println(cegNev+"   "+cegAdoszam);

                String metaDolg=db.showMetaDolg();
                osszDolgozo=db.showAllDolg(cegAdoszam,sql);

                //cegNevLabel.setText(cegNev+"    Adószám:"+cegAdoszam);
                // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                dolgText.append(osszDolgozo);

            }
        });

        fizRend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dolgText.setText("");
                sql ="select * from dolgozo where cegadoszam=? order by bruttober";
                cegAdoszam=db.cegAdoszam(cegNev);

                System.out.println(cegNev+"   "+cegAdoszam);

                String metaDolg=db.showMetaDolg();
                osszDolgozo=db.showAllDolg(cegAdoszam,sql);

               // cegNevLabel.setText(cegNev+"    Adószám:"+cegAdoszam);
                // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                dolgText.append(osszDolgozo);

            }
        });

        fizKozott.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tolCheck = fizTol.getText().toString();
                String igCheck = fizIg.getText().toString();

                if(!tolCheck.isEmpty() && !igCheck.isEmpty()) {
                    int tolInt = Integer.parseInt(fizTol.getText());
                    int igInt = Integer.parseInt(fizIg.getText());

                    if(tolInt <= igInt) {
                        dolgText.setText("");
                        sql = "select * from dolgozo where cegadoszam=? and bruttober between " + tolInt + " and " + igInt;
                        cegAdoszam = db.cegAdoszam(cegNev);

                        System.out.println(cegNev + "   " + cegAdoszam);

                        String metaDolg = db.showMetaDolg();
                        osszDolgozo = db.showAllDolg(cegAdoszam, sql);

                        cegNevLabel.setText(cegNev + "    Adószám:" + cegAdoszam);
                        // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                        dolgText.append(osszDolgozo);
                    } else JOptionPane.showMessageDialog(null,"Kisebb szám elől");
                } else JOptionPane.showMessageDialog(null,"Valami nem stimmel");
            }
        });

        pdf.addActionListener(new ActionListener() {
>>>>>>> main
            @Override
            public void actionPerformed(ActionEvent e) {
                dolgText.setText("");
                sql ="select * from dolgozo where cegadoszam=? order by nev";
                cegAdoszam=db.cegAdoszam(cegNev);

                System.out.println(cegNev+"   "+cegAdoszam);

                String metaDolg=db.showMetaDolg();
                osszDolgozo=db.showAllDolg(cegAdoszam,sql);

                //cegNevLabel.setText(cegNev+"    Adószám:"+cegAdoszam);
                // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                dolgText.append(osszDolgozo);

            }
        });

<<<<<<< HEAD
        fizRend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dolgText.setText("");
                sql ="select * from dolgozo where cegadoszam=? order by bruttober";
                cegAdoszam=db.cegAdoszam(cegNev);

                System.out.println(cegNev+"   "+cegAdoszam);

                String metaDolg=db.showMetaDolg();
                osszDolgozo=db.showAllDolg(cegAdoszam,sql);

               // cegNevLabel.setText(cegNev+"    Adószám:"+cegAdoszam);
                // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                dolgText.append(osszDolgozo);
=======
                String cegNev = cegeValaszto.getSelectedItem().toString();
                String filePath = db.fileChooser();



                String filePathName=filePath;

                if(db.fileNameCheck(filePathName)==false){
                    db.pdfWrite(filePath,cegNev, osszDolgozo);
                }

>>>>>>> main

            }
        });

<<<<<<< HEAD
        fizKozott.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tolCheck = fizTol.getText().toString();
                String igCheck = fizIg.getText().toString();

                if(!tolCheck.isEmpty() && !igCheck.isEmpty()) {
                    int tolInt = Integer.parseInt(fizTol.getText());
                    int igInt = Integer.parseInt(fizIg.getText());

                    if(tolInt <= igInt) {
                        dolgText.setText("");
                        sql = "select * from dolgozo where cegadoszam=? and bruttober between " + tolInt + " and " + igInt;
                        cegAdoszam = db.cegAdoszam(cegNev);

                        System.out.println(cegNev + "   " + cegAdoszam);

                        String metaDolg = db.showMetaDolg();
                        osszDolgozo = db.showAllDolg(cegAdoszam, sql);

                        cegNevLabel.setText(cegNev + "    Adószám:" + cegAdoszam);
                        // dolgText.append(metaDolg+"\n"+"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"+"\n"+osszDolgozo);
                        dolgText.append(osszDolgozo);
                    } else JOptionPane.showMessageDialog(null,"Kisebb szám elől");
                } else JOptionPane.showMessageDialog(null,"Valami nem stimmel");
            }
        });

        pdf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String filePath = db.fileChooser();

                String filePathName=filePath;

                if(db.fileNameCheck(filePathName)==false){
                    db.pdfWrite(filePath, cegNev, osszDolgozo);
                }


            }
        });


        tablazat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tablazat f = new Tablazat(cegAdoszam,sql);
                f.setSize(800, 400);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                f.show();

=======

        tablazat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tablazat f = new Tablazat(cegAdoszam,sql);
                f.setSize(800, 400);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
                f.show();

>>>>>>> main
            }
        });

    }

}
