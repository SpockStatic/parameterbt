import com.itextpdf.awt.geom.Dimension2D;
import com.itextpdf.text.*;
<<<<<<< HEAD
import com.itextpdf.text.pdf.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

=======
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
>>>>>>> main

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
<<<<<<< HEAD
import java.util.Arrays;
import java.util.List;
=======
>>>>>>> main
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DB extends Component {
    final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    final String URL = "jdbc:derby:parameterDB;create=true";
    final String USERNAME = "";
    final String PASSWORD = "";

    //Létrehozzuk a kapcsolatot (hidat)
    Connection conn = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;

    //Megpróbáljuk életre kelteni
    public DB() {

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("A híd létrejött");
        } catch (SQLException ex) {
            System.out.println("Valami baj van a connection (híd) létrehozásakor.");
            System.out.println("" + ex);
        }

        //Ha életre kelt, csinálunk egy megpakolható teherautót
        if (conn != null) {
            try {
                createStatement = conn.createStatement();
            } catch (SQLException ex) {
                System.out.println("Valami baj van van a createStatament (teherautó) létrehozásakor.");
                System.out.println("" + ex);
            }
        }

        //Megnézzük, hogy üres-e az adatbázis? Megnézzük, létezik-e az adott adattábla.
        try {
            dbmd = conn.getMetaData();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a DatabaseMetaData (adatbázis leírása) létrehozásakor..");
            System.out.println("" + ex);
        }

        try {
            ResultSet rs = dbmd.getTables(null, "APP", "USERS", null);
            if (!rs.next()) {
                // createStatement.execute("drop table dolgozo");
                // createStatement.execute("drop table ceg");

              /* createStatement.execute("CREATE TABLE ceg("+
                            "cegnev VARCHAR(25) NOT NULL," +
                            "varos VARCHAR(25) NOT NULL," +
                            "cim VARCHAR(25) NOT NULL," +
                            "adoszam VARCHAR(25) NOT NULL," +
                            "PRIMARY KEY(adoszam))");

               createStatement.execute("CREATE TABLE dolgozo(" +
                        "nev VARCHAR(50) NOT NULL," +
                        "anyjaneve VARCHAR(50) NOT NULL," +
                        "allampolgarsaga VARCHAR(50) NOT NULL," +
                        "varos VARCHAR(25)NOT NULL," +
                        "cim VARCHAR(25)NOT NULL," +
                        "adoazonosito VARCHAR(25)NOT NULL," +
                        "taj VARCHAR(25)NOT NULL," +
                        "bruttober INT NOT NULL," +
                        "cegadoszam VARCHAR(25)NOT NULL," +
                        "FOREIGN KEY(cegadoszam) REFERENCES ceg(adoszam)" +
                        ")");*/
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van az adattáblák létrehozásakor.");
            System.out.println("" + ex);
        }
    }

    /**
     * Cégek hozzáadása az adatbázishoz
     */
    public void addCeg(String cegnev, String varos, String cim, String adoszam) {
        try {
            String sql = "insert into ceg values (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, cegnev);
            preparedStatement.setString(2, varos);
            preparedStatement.setString(3, cim);
            preparedStatement.setString(4, adoszam);


            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a ceg hozzáadásakor");
            System.out.println("" + ex);
        }
    }

    /**
     * Dolgozók hozzáadása az adatbázishoz
     */
    public void addDogozo(String nev, String anyjaneve, String allampolgarsaga, String varos, String cim,
                          String adoazonosito, String taj, int bruttober, String cegadoszam) {
        try {
            String sql = "insert into dolgozo values (?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, nev);
            preparedStatement.setString(2, anyjaneve);
            preparedStatement.setString(3, allampolgarsaga);
            preparedStatement.setString(4, varos);
            preparedStatement.setString(5, cim);
            preparedStatement.setString(6, adoazonosito);
            preparedStatement.setString(7, taj);
            preparedStatement.setInt(8, bruttober);
            preparedStatement.setString(9, cegadoszam);


            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a ceg hozzáadásakor");
            System.out.println("" + ex);
        }
    }

    /**
     * Dolgozók adatinak módoítása az adatbázisban
     */
    public void updateDolgozo(String nev, String anyjaneve, String allampolgarsaga, String varos, String cim,
                              String adoazonosito, String taj, int bruttober, String cegadoszam) {
        try {
            String sql = "UPDATE dolgozo SET nev=?, anyjaneve=?, allampolgarsaga=?, " +
                    "varos=?, cim=?, adoazonosito=?, bruttober=?, cegadoszam=?" +
                    "WHERE taj=?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, nev);
            preparedStatement.setString(2, anyjaneve);
            preparedStatement.setString(3, allampolgarsaga);
            preparedStatement.setString(4, varos);
            preparedStatement.setString(5, cim);
            preparedStatement.setString(6, adoazonosito);
            preparedStatement.setInt(7, bruttober);
            preparedStatement.setString(8, cegadoszam);
            preparedStatement.setString(9, taj);


            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a ceg hozzáadásakor");
            System.out.println("" + ex);
        }
    }

    /**
     * Dolgozó nevének visszaadása Stringben
     */
    public String dolgozoNeve(String Taj) {
        String sql = "select nev from dolgozo where taj = ?";
        String nev = "";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, Taj);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                nev += rs.getString("nev");
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a név kiolvasásakor");
            System.out.println("" + ex);
        }

        return nev;
    }

<<<<<<< HEAD
    /**
     * Dolgozó törlése
     */
    public void torolDolgozo(String Taj) {
        try {
            String sql = "Delete from dolgozo where taj=?";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, Taj);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Hiba a dolgozó törlésekor " + e);
        }
    }

    /**
     * Visszadja Stringben a céggeket fromázva
     */
=======
   /**Dolgozó törlése*/
   public void torolDolgozo(String Taj) {
       try {
           String sql="Delete from dolgozo where taj=?";
          PreparedStatement preparedStatement = conn.prepareStatement(sql);
           preparedStatement.setString(1,Taj);
           preparedStatement.execute();
       } catch (SQLException e){
           System.out.println("Hiba a dolgozó törlésekor "+e);
       }
   }

    /** Visszadja Stringben a céggeket fromázva*/
>>>>>>> main
    public String showAllCeg() {
        String all = "";
        String sql = "select * from ceg";
        String cegek = "";
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()) {
                String Cegnev = rs.getString("cegnev");
                String Varos = rs.getString("varos");
                String Cim = rs.getString("cim");
                String Adoszam = rs.getString("adoszam");
                System.out.printf("%-20s%-20s%-20s%s", Cegnev, Varos, Cim, Adoszam + "\n");

                cegek += String.format("%-20s%-20s%-20s%s", Cegnev , Varos , Cim , Adoszam + "\n");


<<<<<<< HEAD
=======
               cegek += "Cég neve:"+Cegnev+"\nCím: "+Varos+" "+Cim+"\nAdószám: "+Adoszam+"\n\n";

>>>>>>> main
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println("" + ex);
        }
        return cegek;
    }

<<<<<<< HEAD
    /**Visszadja a cégeket String arrayban*/
    public String [] cegekArray(){
       String sql = "select * from ceg";
       ArrayList<String> list = new ArrayList<>();

        try {
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()) {
                String Cegnev = rs.getString("cegnev");
                String Varos = rs.getString("varos");
                String Cim = rs.getString("cim");
                String Adoszam = rs.getString("adoszam");
                System.out.printf("%-20s%-20s%-20s%s", Cegnev, Varos, Cim, Adoszam + "\n");

              list.add(Cegnev);
              list.add(Varos);
              list.add(Cim);
              list.add(Adoszam);

            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println("" + ex);
        }

        String [] all = new String[list.size()];

        for (int i=0; i<list.size();i++){
            all[i] = list.get(i);
        }

        return all;
    }

    /**
     * Egy dolgozót add vissza ArrayListben
     */
    public ArrayList<String> showOneDolg(String taj) {
=======
    /**Egy dolgozót add vissza ArrayListben */
    public ArrayList<String>showOneDolg(String taj) {
>>>>>>> main
        ArrayList<String> list = new ArrayList<>();
        //"select * from dolgozo where taj=?";

        String sql = "select * from dolgozo where taj=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
<<<<<<< HEAD
            ps.setString(1, taj);
=======
            ps.setString(1,taj);
>>>>>>> main

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("nev"));
                list.add(rs.getString("anyjaneve"));
                list.add(rs.getString("allampolgarsaga"));
                list.add(rs.getString("varos"));
                list.add(rs.getString("cim"));
                list.add(rs.getString("adoazonosito"));
                list.add(rs.getString("taj"));
                int ber = rs.getInt("bruttober");
                String berStr = String.valueOf(ber);
                list.add(berStr);
                list.add(rs.getString("cegadoszam"));


<<<<<<< HEAD
=======


>>>>>>> main
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a összes dolgozó kiolvasásakor!!!");
            System.out.println("" + ex);
        }
        return list;
    }

<<<<<<< HEAD
    /**
     * Visszaadja a dolgozókat Stringben fromázva
     */
=======
    /**Visszaadja a dolgozókat Stringben fromázva*/
>>>>>>> main
    public String showAllDolg(String cegadoszam, String sqlFromMet) {
        String dolgozoko = "";
        //"select * from dolgozo where cegadoszam=?";

        String sql = sqlFromMet;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, cegadoszam);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String Nev = rs.getString("nev");
                String Anyjaneve = rs.getString("anyjaneve");
                String Allampolgarsaga = rs.getString("allampolgarsaga");
                String Varos = rs.getString("varos");
                String Cim = rs.getString("cim");
                String Adoazonosito = rs.getString("adoazonosito");
                String Taj = rs.getString("taj");
                int Bruttober = rs.getInt("bruttober");
                String Cegadoszam = rs.getString("cegadoszam");

                System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%s",
                        Nev, Anyjaneve, Allampolgarsaga, Varos, Cim, Adoazonosito, Taj, Bruttober, Cegadoszam + "\n");


                dolgozoko += "Név:  " + Nev + "\nAnyja neve:  " + Anyjaneve + "\nÁllampolgársága:  " + Allampolgarsaga +
                        "\nCíme:  " + Varos + " " + Cim + "\nAdóazonosító:  " + Adoazonosito + "\nTaj: " + Taj +
                        "\nBruttó bér:  " + Bruttober + "\nCég adószáma:  " + Cegadoszam + "\n\n";

<<<<<<< HEAD
=======

                dolgozoko+= "Név:  "+Nev+"\nAnyja neve:  "+Anyjaneve+"\nÁllampolgársága:  "+Allampolgarsaga+
                                "\nCíme:  "+Varos+" "+Cim+"\nAdóazonosító:  "+Adoazonosito+"\nTaj: "+Taj+
                                "\nBruttó bér:  "+Bruttober+"\nCég adószáma:  "+Cegadoszam+"\n\n";


>>>>>>> main

            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a összes dolgozó kiolvasásakor!!!");
            System.out.println("" + ex);
        }
        return dolgozoko;
    }

<<<<<<< HEAD
    /**
     * Dolgozók String arrayba gyűjtése
     */
    public String[] dolgArray(String adoszam) {
        ArrayList<String> list = new ArrayList<>();
        String all = "";
        String sql = "select nev, taj from dolgozo where cegadoszam=?";
=======
    /**Dolgozók String arrayba gyűjtése*/
    public String []dolgArray(String adoszam) {
        ArrayList<String> list = new ArrayList<>();
        String all = "";
        String sql = "select nev, taj from dolgozo where cegadoszam=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,adoszam);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nev = rs.getString("nev");
                String taj = rs.getString("taj");
                list.add(nev +"  Taj: "+taj);
            }

        } catch (SQLException ex) {
            System.out.println("Valami baj van a dolgozók kiolvasásakor");
            System.out.println("" + ex);
        }

        String [] dogNev = new String[list.size()];

        for(int i=0; i<list.size();i++){
            dogNev[i] = list.get(i);
        }

        return dogNev;
    }


   /** Visszaadja a dolgozókat Stringben [][]  Táblázathoz*/
    public String [][] showAllDolgJTable(String cegadoszam, String sql) {
        ArrayList<String> list = new ArrayList<>();
        //String sql = "select * from dolgozo where cegadoszam=?";
>>>>>>> main
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, adoszam);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nev = rs.getString("nev");
                String taj = rs.getString("taj");
                list.add(nev + "    Taj: " + taj);
            }

        } catch (SQLException ex) {
            System.out.println("Valami baj van a dolgozók kiolvasásakor");
            System.out.println("" + ex);
        }

        String[] dogNev = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            dogNev[i] = list.get(i);
        }

        return dogNev;
    }


    /**
     * Visszaadja a dolgozókat Stringben [][]  Táblázathoz
     */
    public String[][] showAllDolgJTable(String cegadoszam, String sql) {
        ArrayList<String> list = new ArrayList<>();
        //String sql = "select * from dolgozo where cegadoszam=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, cegadoszam);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String Nev = rs.getString("nev");
                String Anyjaneve = rs.getString("anyjaneve");
                String Allampolgarsaga = rs.getString("allampolgarsaga");
                String Varos = rs.getString("varos");
                String Cim = rs.getString("cim");
                String Adoazonosito = rs.getString("adoazonosito");
                String Taj = rs.getString("taj");
                int Bruttober = rs.getInt("bruttober");
                String Cegadoszam = rs.getString("cegadoszam");

                list.add(Nev);
                list.add(Anyjaneve);
                list.add(Allampolgarsaga);
                list.add(Varos);
                list.add(Cim);
                list.add(Adoazonosito);
                list.add(Taj);
                list.add(String.valueOf(Bruttober));
                list.add(Cegadoszam);

            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a összes dolgozó kiolvasásakor!!!");
            System.out.println("" + ex);
        }

<<<<<<< HEAD
        int count = 0;
        String[] stringArray = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            stringArray[i] = list.get(i);
        }


        String[][] strtwoDim = new String[stringArray.length / 9][9];

        for (int i = 0; i < 2; i++) {
=======
        int count=0;
        String[] stringArray = new String[list.size()];
        for (int i=0; i<list.size();i++){
            stringArray[i]=list.get(i);
        }


        String [][] strtwoDim = new String[stringArray.length/9][9];

        for(int i=0;i<2;i++) {
>>>>>>> main

            for (int j = 0; j < 9; j++) {

                if (count == stringArray.length) break;

                strtwoDim[i][j] = stringArray[count];

<<<<<<< HEAD
                count++;
=======
              count++;
>>>>>>> main

            }

        }
        return strtwoDim;
    }

/*public void delCeg(){
        try {
          createStatement.execute("delete from ceg where adoszam='8998'");
        }catch (Exception e){System.out.println("Törlési hiba cég:  "+e);}

}
public  void delDolg(){
        try {
            createStatement.execute("delete from dolgozo where taj='123456'");
        }catch (Exception e){
            System.out.println("Törlési hiba dolgozó: "+e);
        }
}

    public  void alterDolg(){
        try {
            createStatement.execute("alter table dolgozo add primary key(taj) ");
        }catch (Exception e){
            System.out.println("Módosítási hiba: "+e);
        }
    }*/

    /**
     * Cégnevek String arrayba gyűjtése
     */
    public String[] cegArray() {
        String all = "";
        String sql = "select cegnev from ceg";
        ArrayList<String> list = new ArrayList<>();
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()) {
                String Cegnev = rs.getString("cegnev");
                list.add(Cegnev);
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println("" + ex);
        }

        String[] cegek = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            cegek[i] = list.get(i);
        }

        return cegek;
    }

    /**
     * Visszaadja a cég adószámát
     */
    public String cegAdoszam(String cegnevString) {

        String sql = "select adoszam from ceg where cegnev = ?";
        String cegAdo = "";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, cegnevString);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                cegAdo += rs.getString("adoszam");
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a cegAdoszam kiolvasásakor");
            System.out.println("" + ex);
        }

        return cegAdo;
    }

    /**
     * Metadata kiolvasása Cég
     */
    public String showMetaCeg() {
        String sql = "select * from ceg";
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        String metaCeg = "";
        try {
            rs = createStatement.executeQuery(sql);
            rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int x = 1; x <= columnCount; x++) {
                System.out.printf("%-20s", rsmd.getColumnName(x));
                metaCeg += String.format("%-30s", rsmd.getColumnName(x));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(" ");

        return metaCeg;
    }

    /**
     * Metadata kiolvasása Dolgozó
     */
    public String showMetaDolg() {
        String sql = "select * from dolgozo";
        String metaDolg = "";
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        try {
            rs = createStatement.executeQuery(sql);
            rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int x = 1; x <= columnCount; x++) {
                System.out.printf("%-20s", rsmd.getColumnName(x));
                metaDolg += String.format("%-30s", rsmd.getColumnName(x));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(" ");
        return metaDolg;
    }

<<<<<<< HEAD
    public String splitString(String str) {

        String taj = "";
=======
    public String splitString(String str){

        String taj="";

        StringBuffer alpha = new StringBuffer(),
                num = new StringBuffer(),
                special = new StringBuffer();
        for (int i=0; i<str.length(); i++)
        {
            if (Character.isDigit(str.charAt(i)))
                num.append(str.charAt(i));
            else if(Character.isAlphabetic(str.charAt(i)))
                alpha.append(str.charAt(i));
            else
                special.append(str.charAt(i));
        }
       return taj+=num;
    }


   /** File mentéséhez útvonal választás*/
    public String fileChooser() {
>>>>>>> main

        StringBuffer alpha = new StringBuffer(),
                num = new StringBuffer(),
                special = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i)))
                num.append(str.charAt(i));
            else if (Character.isAlphabetic(str.charAt(i)))
                alpha.append(str.charAt(i));
            else
                special.append(str.charAt(i));
        }
        return taj += num;
    }


    /**
     * File mentéséhez útvonal választás
     */
    public String fileChooser() {

        String fp = "";
        try {
            JFileChooser open = new JFileChooser();
            open.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            int option = open.showOpenDialog(this);
            File filepath = new File(open.getSelectedFile().getPath());
            fp += filepath;

        } catch (Exception e) {
<<<<<<< HEAD
            System.out.println("Nincs ilyen útvonal" + e);
=======
            System.out.println("Nincs ilyen útvonal"+e);
>>>>>>> main
        }

        return fp;
    }

<<<<<<< HEAD
    /**
     * File név ellenőrzése, hogy létezik-e már ilyen
     */
    public boolean fileNameCheck(String fileNamePath) {
        boolean gotName = false;
        try {
            Scanner scan = new Scanner(new File(fileNamePath + ".pdf"));
            JOptionPane.showMessageDialog(null, "A file név már létezik. Próbálja újra!");
            gotName = true;
=======
    /**File név ellenőrzése, hogy létezik-e már ilyen*/
    public boolean fileNameCheck(String fileNamePath){
       boolean gotName=false;
        try{
           Scanner scan = new Scanner(new File(fileNamePath+".pdf"));
            JOptionPane.showMessageDialog(null,"A file név már létezik. Próbálja újra!");
            gotName = true;

        }catch(FileNotFoundException e){System.out.println("Nincs ilyen");}

        return gotName;
    }

    /**PDF létrehozása és írása céghez */
    public void pdfWrite(String filepath){
       if(!filepath.isEmpty()){
       Document doc = new Document();
        try
        {
            PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filepath+".pdf"));
            System.out.println("PDF created.");
>>>>>>> main

        } catch (FileNotFoundException e) {
            System.out.println("Nincs ilyen");
        }

<<<<<<< HEAD
        return gotName;
    }
=======
            doc.add(new Paragraph("Paraméter Bt.\n\nCégek listája:\n\n"+showAllCeg()));
>>>>>>> main

    /**
     * PDF létrehozása és írása céghez
     */
    public void pdfWrite(String filepath) {
        if (!filepath.isEmpty()) {
            Document doc = new Document();

            try {
                PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filepath + ".pdf"));
                System.out.println("PDF created.");

<<<<<<< HEAD
                doc.open();

                doc.add(new Paragraph("Paraméter Bt.\n\nCégek listája:\n\n" + showAllCeg()));

                doc.close();

                writer.close();

                Desktop.getDesktop().open(new File(filepath + ".pdf"));
            } catch (DocumentException ee) {
                ee.printStackTrace();
            } catch (FileNotFoundException ffe) {
                ffe.printStackTrace();
            } catch (Exception fe) {
                System.out.println(fe);
            }
        } else JOptionPane.showMessageDialog(null, "Nincs útvonal kiválasztva!!!");
    }

    /**
     * Pdf létrehozása és írása cégh dolgozóihoz
     */
    public void pdfWrite(String filepath, String cegNeve, String adat) {

        if (!filepath.isEmpty()) {
            Document doc = new Document();
            try {
                PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filepath + ".pdf"));
                System.out.println("Document created.");

                doc.open();

                doc.add(new Paragraph(cegNeve + "\n\nDolgozók listája:\n\n" + adat));

                doc.close();

                writer.close();

                Desktop.getDesktop().open(new File(filepath + ".pdf"));
            } catch (DocumentException ee) {
                ee.printStackTrace();
            } catch (FileNotFoundException ffe) {
                ffe.printStackTrace();
            } catch (Exception fe) {
                System.out.println(fe);
            }
        } else JOptionPane.showMessageDialog(null, "Nincs útvonal kiválasztva!!!");
=======
            Desktop.getDesktop().open(new File(filepath+".pdf"));
        }
        catch (DocumentException ee)
        {
            ee.printStackTrace();
        }
        catch (FileNotFoundException ffe)
        {
            ffe.printStackTrace();
        }catch (Exception fe){
            System.out.println(fe);
        }
       }else JOptionPane.showMessageDialog(null,"Nincs útvonal kiválasztva!!!");
    }

    /**Pdf létrehozása és írása cégh dolgozóihoz */
   public void pdfWrite(String filepath,String cegNeve, String adat){

       if(!filepath.isEmpty()) {
           Document doc = new Document();
           try {
               PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filepath +".pdf"));
               System.out.println("Document created.");

               doc.open();

               doc.add(new Paragraph(cegNeve + "\n\nDolgozók listája:\n\n" + adat));

               doc.close();

               writer.close();

               Desktop.getDesktop().open(new File(filepath + ".pdf"));
           } catch (DocumentException ee) {
               ee.printStackTrace();
           } catch (FileNotFoundException ffe) {
               ffe.printStackTrace();
           } catch (Exception fe) {
               System.out.println(fe);
           }
       }else JOptionPane.showMessageDialog(null,"Nincs útvonal kiválasztva!!!");
>>>>>>> main
    }
    /**Nem az igazi*/
    @Deprecated

    public void pdfTable(String filePath) {

        String [] cegMind = cegekArray();
        for (int i=0;i<cegMind.length;i++){
            System.out.println(cegMind[i]);
        }

<<<<<<< HEAD
        try {
            Document document = new Document();

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath+".pdf"));

            document.open();

            document.add(new Paragraph("Cégek listája"));


            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10f);
            table.setSpacingAfter(10f);

            List<PdfPRow> listRow = table.getRows();

            float[] columnWidths = {1f, 1f, 1f, 1f};
            table.setWidths(columnWidths);

            PdfPCell cells0[] = new PdfPCell[4];
            PdfPRow row0 = new PdfPRow(cells0);

            cells0[0] = new PdfPCell(new Paragraph("Cégnév"));
            cells0[0].setBorderColor(BaseColor.BLUE);
            cells0[0].setPaddingLeft(20);
            cells0[0].setHorizontalAlignment(Element.ALIGN_CENTER);
            cells0[0].setVerticalAlignment(Element.ALIGN_MIDDLE);

            cells0[1] = new PdfPCell(new Paragraph("Város"));
            cells0[1].setBorderColor(BaseColor.BLUE);
            cells0[1].setPaddingLeft(20);
            cells0[1].setHorizontalAlignment(Element.ALIGN_CENTER);
            cells0[1].setVerticalAlignment(Element.ALIGN_MIDDLE);

            cells0[2] = new PdfPCell(new Paragraph("Cim"));
            cells0[2].setBorderColor(BaseColor.BLUE);
            cells0[2].setPaddingLeft(20);
            cells0[2].setHorizontalAlignment(Element.ALIGN_CENTER);
            cells0[2].setVerticalAlignment(Element.ALIGN_MIDDLE);

            cells0[3] = new PdfPCell(new Paragraph("Adószám"));
            cells0[3].setBorderColor(BaseColor.BLUE);
            cells0[3].setPaddingLeft(20);
            cells0[3].setHorizontalAlignment(Element.ALIGN_CENTER);
            cells0[3].setVerticalAlignment(Element.ALIGN_MIDDLE);


            PdfPCell cells1[] = new PdfPCell[4];
            PdfPRow row1 = new PdfPRow(cells1);


            for(int i=0; i<cells1.length;i++){
                cells1[i] = new PdfPCell(new Paragraph(cegMind[i]));
            }



            PdfPCell cells2[] = new PdfPCell[4];
            PdfPRow row2 = new PdfPRow(cells2);
            for(int i=0; i<cells2.length;i++){
                cells2[i] = new PdfPCell(new Paragraph(cegMind[i+4]));
            }



            listRow.add(row0);
            listRow.add(row1);
            listRow.add(row2);

            document.add(table);


            document.close();

            writer.close();
            Desktop.getDesktop().open(new File(filePath + ".pdf"));
        }catch (Exception e){
            System.out.println("PdfTable  "+e);
        }
    }


        /*JTable xls-be írása*/
    public void exportXls(JTable table, String fileNamePath){
        try
        {
            TableModel m = table.getModel();
            FileWriter fw = new FileWriter(fileNamePath+".xls", StandardCharsets.ISO_8859_1);

            for(int i = 0; i < m.getColumnCount(); i++){
                fw.write(m.getColumnName(i) + "\t");
            }
            fw.write("\n");
            for(int i=0; i < m.getRowCount(); i++) {
                for(int j=0; j < m.getColumnCount(); j++) {
                    fw.write(m.getValueAt(i,j).toString()+"\t");
                }
                fw.write("\n");
            }
            fw.close();
            Desktop.getDesktop().open(new File(fileNamePath + ".xls"));
        }
=======

        /*JTable xls-be írása*/
    public void exportXls(JTable table, String fileNamePath){
        try
        {
            TableModel m = table.getModel();
            FileWriter fw = new FileWriter(fileNamePath+".xls", StandardCharsets.ISO_8859_1);

            for(int i = 0; i < m.getColumnCount(); i++){
                fw.write(m.getColumnName(i) + "\t");
            }
            fw.write("\n");
            for(int i=0; i < m.getRowCount(); i++) {
                for(int j=0; j < m.getColumnCount(); j++) {
                    fw.write(m.getValueAt(i,j).toString()+"\t");
                }
                fw.write("\n");
            }
            fw.close();
            Desktop.getDesktop().open(new File(fileNamePath + ".xls"));
        }
>>>>>>> main
        catch(IOException e){ System.out.println(e); }
    }




}