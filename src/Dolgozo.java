import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Dolgozo extends JFrame {

    private JLabel nevLabel;
    private JLabel anyjaNeveLabel;
    private JLabel allampolgarLabel;
    private JLabel varosLebel;
    private JLabel cimLabel;
    private JLabel adoazLabel;
    private JLabel tajLabel;
    private JLabel bruttoLabel;
    private JLabel cegAdoszamLabel;
    private JLabel figyelmeztetMod;
    private JLabel figyelmeztetTor;


    private JTextField nevText;
    private JTextField anyjaNeveText;
    private JTextField allampolgarText;
    private JTextField varosText;
    private JTextField cimText;
    private JTextField adoazText;
    private JTextField tajText;
    private JTextField bruttoText;
    private JTextField cegAdoszamText;

    private JComboBox dolgCombo;

    private  JCheckBox modCheck;
    private  JCheckBox torCheck;


    private JButton hozzaad;
    private JButton modosit;
    private JButton cegOk;
<<<<<<< HEAD
   // private JButton clear;
=======
    private JButton clear;
>>>>>>> main
    private JButton del;

    public Dolgozo(String adoszam, String cegNev){
        super(cegNev);
        setLayout(null);

        DB db = new DB();
        //Labels

        nevLabel = new JLabel("Név: ");
        nevLabel.setBounds(10,60,100,30);
        add(nevLabel);

        anyjaNeveLabel = new JLabel("Anyja neve: ");
        anyjaNeveLabel.setBounds(10,110,100,30);
        add(anyjaNeveLabel);

        allampolgarLabel = new JLabel("Állampolgársága: ");
        allampolgarLabel.setBounds(10,160,100,30);
        add(allampolgarLabel);

        varosLebel = new JLabel("Város: ");
        varosLebel.setBounds(10,210,100,30);
        add(varosLebel);

        cimLabel = new JLabel("Cím: ");
        cimLabel.setBounds(10,260,100,30);
        add(cimLabel);

        adoazLabel = new JLabel("Adóazonosító: ");
        adoazLabel.setBounds(10,310,100,30);
        add(adoazLabel);

        tajLabel = new JLabel("Taj szám: ");
        tajLabel.setBounds(10,360,100,30);
        add(tajLabel);

        bruttoLabel = new JLabel("Bruttó bér: ");
        bruttoLabel.setBounds(10,410,100,30);
        add(bruttoLabel);

        cegAdoszamLabel = new JLabel("Cég adószám");
        cegAdoszamLabel.setBounds(10, 460,100,30);
        add(cegAdoszamLabel);

     /*   figyelmeztetMod = new JLabel("<html>Az alkalmazott módosításhoz minden mező kitöltése kötelező!</html>");
        figyelmeztetMod.setBounds(425,140,145,60);
        add(figyelmeztetMod);

        figyelmeztetTor = new JLabel("<html>Alkalmazott törléséhez tajszám megadása kötelező!</html>");
        figyelmeztetTor.setBounds(425,240,145,60);
        add(figyelmeztetTor);*/

        // TextFields

        nevText = new JTextField();
        nevText.setBounds(120,60,300,30);
        add(nevText);

        anyjaNeveText = new JTextField();
        anyjaNeveText.setBounds(120,110,300,30);
        add(anyjaNeveText);

        allampolgarText = new JTextField();
        allampolgarText.setBounds(120,160,300,30);
        add(allampolgarText);

        varosText = new JTextField();
        varosText.setBounds(120,210,300,30);
        add(varosText);

        cimText = new JTextField();
        cimText.setBounds(120,260,300,30);
        add(cimText);

        adoazText = new JTextField();
        adoazText.setBounds(120,310,300,30);
        add(adoazText);

        tajText = new JTextField();
        tajText.setBounds(120,360,300,30);
        add(tajText);

        bruttoText = new JTextField();
        bruttoText.setBounds(120,410,300,30);
        add(bruttoText);

        cegAdoszamText = new JTextField(adoszam);
<<<<<<< HEAD
        cegAdoszamText.setBounds(120,460,300,30);
=======
        cegAdoszamText.setBounds(120,410,300,30);
>>>>>>> main
        cegAdoszamText.setEditable(false);
        add(cegAdoszamText);

        // ComboBox

        dolgCombo = new JComboBox(db.dolgArray(adoszam));
<<<<<<< HEAD
        dolgCombo.setBounds(10,10,250,30);
=======
        dolgCombo.setBounds(425,10,145,30);
>>>>>>> main
        add(dolgCombo);

        // CheckBox

     /*   modCheck = new JCheckBox(" Módosítás", false);
        modCheck.setBounds(10,210,145,30);
        add(modCheck);

        torCheck = new JCheckBox(" Törlés", false);
        torCheck.setBounds(425,310,145,30);
        add(torCheck);*/


        //Button's

        cegOk = new JButton("Kiválaszt");
<<<<<<< HEAD
        cegOk.setBounds(275,10,145,30);
=======
        cegOk.setBounds(425,60,145,30);
>>>>>>> main
        add(cegOk);

        hozzaad=new JButton("Hozzáadás");
        hozzaad.setBounds(10,500,100,30);
        add(hozzaad);

        modosit = new JButton("Módosít");
        modosit.setBounds(160,500,100,30);
        add(modosit);

        del=new JButton("Törlés");
        del.setBounds(310,500,100,30);
        add(del);

<<<<<<< HEAD
      /*  clear = new JButton("Clear");
        clear.setBounds(460,500,100,30);
        add(clear);*/
=======
        clear = new JButton("Clear");
        clear.setBounds(460,500,100,30);
        add(clear);
>>>>>>> main

        //Action Listeners

        cegOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String nameTaj = dolgCombo.getSelectedItem().toString();
                System.out.println(db.splitString(nameTaj));
                ArrayList<String> list = db.showOneDolg(db.splitString(nameTaj));
                nevText.setText(list.get(0));
                anyjaNeveText.setText(list.get(1));
                allampolgarText.setText(list.get(2));
                varosText.setText(list.get(3));
                cimText.setText(list.get(4));
                adoazText.setText(list.get(5));
                tajText.setText(list.get(6));
                bruttoText.setText(list.get(7));


            }
        });


       hozzaad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String nev = nevText.getText();
                String anyja = anyjaNeveText.getText();
                String allam=allampolgarText.getText();
                String varos = varosText.getText();
                String cim = cimText.getText();
                String ado=adoazText.getText();
                String taj = tajText.getText();
                String  bruttoStr = bruttoText.getText();
                int brutto =0;
                if(!bruttoStr.equals("")){
                   brutto = Integer.parseInt(bruttoStr);
                }

                String cegAdo = cegAdoszamText.getText();

                if(!nev.isEmpty() && !anyja.isEmpty() && !allam.isEmpty() && !varos.isEmpty()
                    && !cim.isEmpty() && !ado.isEmpty() && !taj.isEmpty() && !bruttoStr.isEmpty() && !cegAdo.isEmpty()
                    && !nev.equals("") && !anyja.equals("") && !allam.equals("") && !varos.equals("") && !cim.equals("")
                    && !ado.equals("") && !taj.equals("") && !bruttoStr.equals("") && !cegAdo.equals("")) {

                   int answear = JOptionPane.showConfirmDialog(null,"Biztos menti a dolgozót?","Mentés",JOptionPane.YES_NO_CANCEL_OPTION);

                   if(answear == 0){

                       db.addDogozo(nev,anyja,allam,varos,cim,ado,taj,brutto,cegAdo);
                       JOptionPane.showMessageDialog(null," A "+nev+" hozzá lett adva az adatbázishoz");

                   }


                }else JOptionPane.showMessageDialog(null,"Az összes mező kitőltése kötelező");

            }
        });

      /* clear.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               nevText.setText(null);
               anyjaNeveText.setText(null);
               allampolgarText.setText(null);
               varosText.setText(null);
               cimText.setText(null);
               adoazText.setText(null);
               tajText.setText(null);
               bruttoText.setText(null);
               cegAdoszamText.setText(null);
           }
       });*/

       modosit.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {

                   String nev = nevText.getText();
                   String anyja = anyjaNeveText.getText();
                   String allam=allampolgarText.getText();
                   String varos = varosText.getText();
                   String cim = cimText.getText();
                   String ado=adoazText.getText();
                   String taj = tajText.getText();
                   String  bruttoStr = bruttoText.getText();
                   int brutto =0;
                   if(!bruttoStr.equals("")) {
                       brutto = Integer.parseInt(bruttoStr);
                   }

                       String cegAdo = cegAdoszamText.getText();
                       String nevEllenorzes = db.dolgozoNeve(taj);

                       if (!nev.isEmpty() && !anyja.isEmpty() && !allam.isEmpty() && !varos.isEmpty()
                               && !cim.isEmpty() && !ado.isEmpty() && !taj.isEmpty() && !bruttoStr.isEmpty() && !cegAdo.isEmpty()
                               && !nev.equals("") && !anyja.equals("") && !allam.equals("") && !varos.equals("") && !cim.equals("")
                               && !ado.equals("") && !taj.equals("") && !bruttoStr.equals("") && !cegAdo.equals("")
                               && !nevEllenorzes.equals("")) {

<<<<<<< HEAD
                           int answear = JOptionPane.showConfirmDialog(null,"Biztos módosítása a dolgozót?","Módosítás",JOptionPane.YES_NO_CANCEL_OPTION);

                           if(answear == 0) {

                               db.updateDolgozo(nev, anyja, allam, varos, cim, ado, taj, brutto, cegAdo);
                               JOptionPane.showMessageDialog(null, " A " + nev + " módosítva lett az adatbázisban");
                           }

                       } else
                           JOptionPane.showMessageDialog(null, "Az összes mező kitőltése kötelező vagy nincs ilyen személy az adatbázisban");
=======
                           db.updateDolgozo(nev, anyja, allam, varos, cim, ado, taj, brutto, cegAdo);
                           JOptionPane.showMessageDialog(null, " A " + nev + " módosítva lett az adatbázisban");
                       } else
                           JOptionPane.showMessageDialog(null, "Az összes mező kitőltése kötelező vagy nincs ilyen személy az adatbázisban");


           }
       });

        del.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(torCheck.isSelected() && !modCheck.isSelected()){
                    String Taj = tajText.getText();
                    String nev = db.dolgozoNeve(Taj);
>>>>>>> main


                    if(!Taj.isEmpty() && !Taj.equals("") && !nev.equals("")){
                        db.torolDolgozo(Taj);
                        JOptionPane.showMessageDialog(null, "A "+ nev +" törlése sikeres");
                    }else JOptionPane.showMessageDialog(null, "A Taj mező kitőltése kötelező vagy nincs ilyen személy az adatbázisban");

                }
            }
        });

        del.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                    String Taj = tajText.getText();
                    String nev = db.dolgozoNeve(Taj);


                    if(!Taj.isEmpty() && !Taj.equals("") && !nev.equals("")){

                        int answear = JOptionPane.showConfirmDialog(null,"Biztos törli a dolgozót?","Törlés",JOptionPane.YES_NO_CANCEL_OPTION);

                        if(answear == 0) {
                            db.torolDolgozo(Taj);
                            JOptionPane.showMessageDialog(null, "A " + nev + " törlése sikeres");
                        }

                    }else JOptionPane.showMessageDialog(null, "A Taj mező kitőltése kötelező vagy nincs ilyen személy az adatbázisban");

                }

        });

    }

}
