
import java.awt.*;
import java.io.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Menu extends JFrame {

private JLabel Parameter;
private JButton ceg;
private JButton cegLista;
private JButton dolgozo;
private JButton Dolglista;
private JComboBox cegValaszt;
private JButton cegOk;
String adoszam;
String cegNev;

    public Menu(){
        super("Paraméter Bt.");
        setLayout(null);
        DB db = new DB();


        Parameter= new JLabel("<html>P a r a m é t e r Bt</html>");
        Parameter.setFont(new Font("Serif", Font.BOLD, 21));
        Parameter.setBounds(250,10,25,280);
        add(Parameter);


        ceg = new JButton("Cég hozzáadása");
        ceg.setBounds(10,30,160,30);
        add(ceg);

        cegLista = new JButton("Cégek listázása");
        cegLista.setBounds(10,80,160,30);
        add(cegLista);

        cegValaszt = new JComboBox(db.cegArray());
        cegValaszt.setBounds(10,130,160,30);
        add(cegValaszt);

        cegOk = new JButton("OK");
        cegOk.setBounds(180,130,60,30);
        add(cegOk);

        dolgozo = new JButton("Dolgozok kezelése");
        dolgozo.setBounds(10,180,160,30);
        dolgozo.setEnabled(false);
        add(dolgozo);

        Dolglista = new JButton("Dolgozok listázása");
        Dolglista.setBounds(10,230,160,30);
        Dolglista.setEnabled(false);
        add(Dolglista);


        ceg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                CegHozzadasa ch = new CegHozzadasa();
                ch.setVisible(true);
                ch.setSize(450,300);
                ch.setLocationRelativeTo(null);
                ch.setResizable(false);
                ch.show();

            }
        });

        cegLista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               String filePath=db.fileChooser();
               if(db.fileNameCheck(filePath)==false){
<<<<<<< HEAD
                   db.pdfTable(filePath);
=======
                   db.pdfWrite(filePath);
>>>>>>> main
               }

            }

        });

        dolgozo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Dolgozo dolgozo = new Dolgozo(adoszam, cegNev);
                dolgozo.setVisible(true);
<<<<<<< HEAD
                dolgozo.setSize(450,600);
=======
                dolgozo.setSize(600,600);
>>>>>>> main
                dolgozo.setLocationRelativeTo(null);
                dolgozo.setResizable(false);
                dolgozo.show();
            }
        });

        Dolglista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                DolgozoLista dl = new DolgozoLista(adoszam,cegNev);
                dl.setVisible(true);
                dl.setSize(642,510);
                dl.setLocationRelativeTo(null);
                dl.setResizable(false);
                dl.show();

            }
        });

        cegOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cegNev =cegValaszt.getSelectedItem().toString();
                adoszam=db.cegAdoszam(cegNev);

                dolgozo.setEnabled(true);
                Dolglista.setEnabled(true);

            }
        });
    }


}
